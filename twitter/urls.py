from django.urls import path
#from . import views
from .views.account.account import *
from .views.timeLine.TwitApi import *
from .views.account.profile import *


urlpatterns = [
    #path('api/profile/', views.UserListCreate.as_view()),
    path('api/login/', login),
    path('api/logout/',logout),
    path('api/getAllTimeLine/', getAllTimeline.as_view()),
    path('api/uploadTwit/', UploadTwit.as_view()),
    path('api/getUserProfile', Profile.as_view()),
    path('api/idCheck', idCheck.as_view()),
    path('api/signUp',signUp.as_view()),
]