from rest_framework import serializers
from django.contrib.auth.models import User

from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('__all__')

class TwitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Twit
        fields = ('__all__')
    
    def list(self):
        #queryset = Twit.objects.raw("select * from Twit")
        #print("end")
        queryset = Twit.objects.all()
        serializer = TwitSerializer(queryset, many=True)
        return serializer.data

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('__all__')

class getUserProfile(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('__all__')

    def findOneUser(self, id):
        #queryset = UserProfile.objects.raw("select * from user_profile where id = '" +  id + "'")
        queryset = UserProfile.objects.filter(id = id)
        serializer = UserProfileSerializer(queryset, many=True)
        return serializer.data

    def findUsername(self,username):
        queryset = User.objects.filter(username=username)
        #queryset = User.objects.raw("select * from auth_user where username = '" +  username + "'")
        serializer = UserSerializer(queryset, many=True)
        print(serializer)
        return serializer.data
    
    def insertOneUser(self,userId,userPassword):
        user = User.objects.create_user(userId, password=userPassword)
        user.save()
        queryset=User.objects.filter(username = userId)
        serializer = UserSerializer(queryset, many=True)
        userProfile = UserProfile.objects.create(id=serializer.data[0]['id'], name=serializer.data[0]['username'])
        userProfile.save()
