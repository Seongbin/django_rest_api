from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from twitter.serializers import *
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser

class UploadTwit(APIView):
    serializers_class = TwitSerializer
    permission_class = (IsAuthenticated,)

    def post(self, request, format=None):
        serializer = TwitSerializer(data=request.data.dict())
        #serializer = TwitSerializer(data=request.data)
        #serializer = TwitSerializer( data=request.data)
        print(serializer.is_valid())
        print("serializer-----------------",serializer)
        if serializer.is_valid():
            serializer.save()
            print(serializer.errors)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        print("serializer", serializer)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class getAllTimeline(APIView):

    serializers_class = TwitSerializer
    permission_class = (IsAuthenticated,)
    
    def post(self, request, format=None):
        serializer = TwitSerializer.list(self)
        print(serializer)
        return Response(serializer, status=status.HTTP_200_OK)
