from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view

from twitter.serializers import *

@api_view(['GET'])
def getTimeLineFirst(self):
    return Response(getTimeLineFirst)