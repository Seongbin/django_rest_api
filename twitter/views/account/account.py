from django.shortcuts import render
'''
# Create your views here.
#from django.contrib.auth.models import User
#from twitter.serializers import UserSerializer
#from rest_framework import generics
#from rest_framework.renderers import JSONRenderer
#from rest_framework.response import Response
#from rest_framework.views import APIView

class UserListCreate(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
'''

#Auth Dependency
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from twitter.serializers import *

from ..shared.isPost import *
import json
@csrf_exempt
@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request):
    
    if isPost(request.method) is False:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    username = request.data["username"]
    password = request.data["userpassword"]
    if username is None or password is None:
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY)
    user = authenticate(username=username, password=password)
    if not user:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    token, _ = Token.objects.get_or_create(user=user)
    # 토큰 발행

    # 토큰은 세션(디스크)에 저장
    # 프론트는 토큰을 가지고있다
    # 프론트에서 api를 콜 할때 토큰을 헤더에 달고 리퀘스트 한다
    # api단에서 토큰이 있는지 없는지 확인하고
    # 토큰이 있으면 토큰이 유효기간이 있는데 
    # 보존 기간이 지났는지 안 지났는지 확인
    # 이 조건이 참이라면 리스폰스를 보낸다
    # JWT에 대해서 알아볼 것

    response = {
        'tokenKey':token.key,
        'userNumber' : user.id
    }

    return Response(response,status=status.HTTP_200_OK)

@api_view(['POST'])
@permission_classes((AllowAny,))
def logout(request):
    if isPost(request.method) is False:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    
    try:
        print(request.user)
        print(request.user.auth_token)
        request.user.auth_token.delete()
    except (AttributeError):
        print("err")
        pass

    return Response({'message':"You're logged out."},status=status.HTTP_200_OK)
'''
@csrf_exempt
@api_view(['POST'])
@permission_classes((AllowAny,))
def idCheck( request):

    if isPost(request.method) is False:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    requestId = json.loads(request.body)
    id = requestId['id']
    print(type(id))
    serializers = getUserProfile.findOneUser1(id)

    print(serializers)
    return Response({'abc':'abc'},status = status.HTTP_200_OK)
'''
class idCheck(APIView):
    serializers_class = getUserProfile
    permission_class = (IsAuthenticated,)
    
    def post(self, request, format=None):
        username = self.request.POST.get('userId')
        print('userId',username)
        serializer = getUserProfile.findUsername(self,username)
        return Response(serializer,status=status.HTTP_200_OK)

class signUp(APIView):
    
    def post(self, request, format=None):
        username = self.request.POST.get('userId')
        password = self.request.POST.get('password')
        getUserProfile.insertOneUser(self,username, password)
        return Response(status=status.HTTP_200_OK)