from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from twitter.serializers import *
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser


class Profile(APIView):

    serializers_class = getUserProfile
    permission_class = (IsAuthenticated,)
    
    def get(self, request, format=None):
        id = self.request.GET.get('id')
        serializer = getUserProfile.findOneUser(self,id)
        return Response(serializer, status=status.HTTP_200_OK)
